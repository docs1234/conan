To generate, 

1. Prepare system.  Install enchant and graphviz libraries.  On Debian/Ubuntu
~~~
sudo apt install libenchant-2-2 graphviz
~~~
2. Run `./generate.sh`

Note: `generate.sh` script uses the pip package `virtualenvwrapper` to isolate pip dependencies
