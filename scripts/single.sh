#!/usr/bin/env bash

if [ "$1" = "" ]; then
  echo "Please give argument for version"
  exit 1
fi

VERSION=$1

git clone --depth 1 -b release/$VERSION https://github.com/conan-io/docs.git

source $(which virtualenvwrapper.sh)
mkvirtualenv conan-docs
workon conan-docs
pip install pynvim

cd docs
pip install -r requirements.txt
pip install pynvim colorama conan==$VERSION
make html

cd ..
rm -rf Conan.docset
pip install doc2dash
doc2dash -n Conan --index-page index.html docs/_build/html
cp ../../icons/* Conan.docset
tar -czf Conan.tgz Conan.docset
